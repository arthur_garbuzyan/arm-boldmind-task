import React, {Suspense, useState } from "react";
import { Switch, Router, Route } from "react-router";
import history from "./history";
import Base from "../layouts/base";
import {StepOne, StepTwo, StepThree, StepFour} from "../modules/events";
import App from "../App";
import Page400 from "../modules/404/page404";
import StepsContext from "../context/stepsContext";

const RoutContainer = () => {

    const [formData, setFormData] = useState({});

    return (
        <Suspense fallback={<h2>Loading...</h2>}>
            <StepsContext.Provider value={{formData, setFormData}}>
            <Router history={history}>
                <Switch>
                    <Base exact path='/step-one' component={StepOne}/>
                    <Base exact path='/step-two' component={StepTwo}/>
                    <Base exact path='/step-three' component={StepThree}/>
                    <Base exact path='/step-four' component={StepFour}/>
                    <Base exact path='/' component={StepOne}/>
                    <Route exact path='*' component={Page400}/>
                </Switch>
            </Router>
            </StepsContext.Provider>
        </Suspense>

    )
};

export default RoutContainer;
