import React from "react";
import {Route, withRouter} from "react-router";
import PropTypes from "prop-types";
import './styles.scss';
import NavBar from "./components/navbar";

const Base = props => {
    const {
        component: ChildComponent,
        history,
        ...rest
    } = props;

    return (
        <main>
            <div className='header position-relative'>
                <NavBar/>
            </div>
            <Route
                {...rest}
                render={matchProps => {
                    return <ChildComponent className='container' {...matchProps} />;
                }}
            />
        </main>
    );
};

Base.propTypes = {
    component: PropTypes.any,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }),
};

export default withRouter(Base);