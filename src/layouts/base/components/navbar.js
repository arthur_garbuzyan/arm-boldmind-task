import React from 'react';
import { Link } from 'react-router-dom';

function NavBar() {

    return (
        <div className='navbar-content'>
            <div className="navbar">
                <Link to='/'>Dashboard</Link>
                <Link to='/'>Calendar</Link>
                <Link to='/'>Hotels</Link>
                <Link to='/' className="active">My events</Link>
                <Link to='/'>Notes</Link>
                <Link to='/'>Messages <span className='alert-count'>5</span></Link>
                <Link to='/' className='menu'>My account</Link>
                <a className="icon">&#9776;</a>
            </div>
        </div>
    );
}

export default NavBar;