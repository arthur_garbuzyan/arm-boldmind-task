import React, {useContext} from 'react';
import Steps from '../steps'
import './styles.scss';
import {Select, Button, Input} from '../components/formElements';
import Section from "../components/section";
import FormContent from "../components/formContent";
import history from "../../routes/history";
import HoverTable from "../components/tables/hoverTable";
import StepsContext from "../../context/stepsContext";

function StepThree({match}) {

    const formDataContext = useContext(StepsContext);

    const setContextFormData = (val, index) => {
        const getData = {...formDataContext.formData};
        getData[index] = val;
        formDataContext.setFormData(getData);
    };

    return (
        <div className="container">
            <Steps percents={40} prevStep='step-two' location={match}/>
            <div className='d-flex-align-center'>
                <Section>
                    <div className='content-section'>
                        <FormContent title='Add guest'>
                            <Input label='First name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'guestFirstName')}}/>
                            <Input label='Middle name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'guestMiddleName')}}/>
                            <Input label='Last name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'guestLastName')}}/>
                            <Input label='Phone' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'guestPhone')}}/>
                            <Input label='Email' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'guestEmail')}}/>
                            <Select label='Side' defaultValue='Choose side' classes='form-control' callBack={(val) => {setContextFormData(val, 'guestSide')}}/>
                        </FormContent>
                        <Button classes={'btn-primary btn-bg-green mt-3'} collBack={() => {}} text='Add to list'/>

                        <HoverTable/>

                        <div className='d-flex btn-box'>
                            <Button classes={'btn-link btn-ping'} text='Back' collBack={() => {history.goBack()}}/>
                            <Button classes={'btn-primary btn-bg-ping ml-2'} text='Next' collBack={() => {history.push('/step-four')}}/>
                        </div>
                    </div>
                </Section>
            </div>
        </div>
    );
}

export default StepThree;