import React, {useState, useContext, useEffect} from 'react';
import Steps from '../steps'
import './styles.scss';
import Section from "../components/section";
import {Select, Button, Checkbox, Input} from '../components/formElements';
import FormContent from "../components/formContent";
import history from "../../routes/history";
import StepsContext from "../../context/stepsContext";

function StepTwo({match}) {

    const formDataContext = useContext(StepsContext);

    const [information, setInformation] = useState({
        ceremonyType: '',
        hotel: '',
        checkIn: '',
        checkOut: '',
        roomNumber: '',
        honeymoon: '',
        bookedRoom: '',
    });

    const [error, setError] = useState(false);

    const checkFields = () => {

        for (let index in information) {
            if (!information[index]) {
                setError(true);
                return;
            }
        }
        setError(false);
        formDataContext.setFormData({...formDataContext.formData, ...information});
        return history.push('/step-three')
    };

    return (
        <div className="container">
            <Steps percents={18} prevStep={'step-one'} location={match}/>
            <div className='d-flex-align-center'>
                <Section>
                    <div className='content-section'>
                        {error && <p className='error-alert text-center'>All Fields is require</p>}
                        <FormContent title='Wedding information' classes='d-flex-space-between '>
                            <Select label='Ceremony type' defaultValue='Choose type' callBack={(val) => setInformation({...information, ceremonyType: val})} classes='form-control-big'/>
                            <Select label='Hotel' defaultValue='Choose hotel' callBack={(val) => setInformation({...information, hotel: val})} classes='form-control-big'/>
                            <Select label='Check in data' defaultValue='Choose date' callBack={(val) => setInformation({...information, checkIn: val})} classes='form-control-big'/>
                            <Select label='Check out data' defaultValue='Choose date' callBack={(val) => setInformation({...information, checkOut: val})} classes='form-control-big'/>
                            <div className='d-flex-wrap'>
                                <Checkbox text='Honeymoon' classes='form-control' collBack={() => setInformation({...information, honeymoon: !information.honeymoon})}/>
                                <Checkbox text='Booked room' classes='form-control' collBack={() => setInformation({...information, bookedRoom: !information.bookedRoom})}/>
                            </div>
                        </FormContent>
                        <Input label='Room number' classes='form-control-big' callBack={(val) => setInformation({...information, roomNumber: val})}/>
                        <div className='d-flex btn-box'>
                            <Button classes={'btn-link btn-ping '} text='Back' collBack={() => {history.goBack()}}/>
                            <Button classes={'btn-primary btn-bg-ping ml-2'} text='Next' collBack={() => {checkFields()}}/>
                        </div>
                    </div>
                </Section>
            </div>
        </div>
    );
}

export default StepTwo;