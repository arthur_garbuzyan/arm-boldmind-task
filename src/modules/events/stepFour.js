import React, {useContext, useState} from 'react';
import Steps from '../steps'
import './styles.scss';
import { Button } from '../components/formElements';
import Section from "../components/section";
import history from "../../routes/history";
import VendorsTable from "../components/tables/vendorsTable";
import Modal from "../components/modals/modal";
import StepsContext from "../../context/stepsContext";

function StepFour({match}) {

    const formDataContext = useContext(StepsContext);
    const [modalIsShowing, setModalIsShowing] = useState(false);

    const createEvent = () => {
        const finalFormData = formDataContext.formData;
        window.localStorage.setItem('eventInformation', JSON.stringify(finalFormData));
        setModalIsShowing(true)
    };

    return (
        <div>
            <div className="container">
                <Steps percents={75} prevStep={'step-three'} location={match}/>
                <div className='d-flex-align-center'>
                    <div className='d-flex-wrap mt-2'>
                        <Section>
                            <h2 className='form-title'>Vendors</h2>
                        </Section>
                        <VendorsTable/>
                        <Section>
                            <div className='content-section'>
                                <div className='d-flex btn-box'>
                                    <Button classes={'btn-link btn-ping'} text='Back' collBack={() => {history.goBack()}}/>
                                    <Button classes={'btn-primary btn-bg-ping ml-2'} text='Create event' collBack={() => {createEvent()}}/>
                                </div>
                            </div>
                        </Section>
                    </div>
                </div>
            </div>
            <Modal show={modalIsShowing} close={() => setModalIsShowing(false)}/>
        </div>
    );
}

export default StepFour;