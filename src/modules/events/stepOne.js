import React, {useState, useEffect, useContext} from 'react';
import Steps from '../steps'
import {Select, Button, Input} from '../components/formElements';
import './styles.scss';
import Section from "../components/section";
import FormContent from "../components/formContent";
import history from "../../routes/history";
import StepsContext from "../../context/stepsContext";

function StepOne({match}) {

    const formDataContext = useContext(StepsContext);

    const setContextFormData = (val, index) => {
        const getData = {...formDataContext.formData};
        getData[index] = val;
        formDataContext.setFormData(getData);
    };

    return (
        <div className="container">
            <Steps percents={18} prevStep={'step-one'} location={match}/>
            <div className='d-flex-align-center'>
                <Section>
                    <div className='content-section'>
                        <FormContent title='Groom and bride account email'>
                            <Input label='Email' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'email')}}/>
                            <Input label='Wedding Name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'weddingName')}}/>
                        </FormContent>

                        <FormContent title='Groom Information'>
                            <Input label='First name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'firstName')}}/>
                            <Input label='Middle name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'middleName')}}/>
                            <Input label='Last name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'lastName')}}/>
                            <Select label='Date of bird' defaultValue='Choose Date' classes='form-control' callBack={(val) => {setContextFormData(val, 'dateOfBird')}}/>
                            <Input label='Phone number' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'phoneNumber')}}/>
                            <Select label='Gender' defaultValue='Choose gender' classes='form-control' callBack={(val) => {setContextFormData(val, 'gender')}}/>
                        </FormContent>

                        <FormContent title='Bride Information'>
                            <Input label='First name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'brideFirstName')}}/>
                            <Input label='Middle name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'brideMiddleName')}}/>
                            <Input label='Last name' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'brideLastName')}}/>
                            <Select label='Date of bird' defaultValue='Choose Date' classes='form-control' callBack={(val) => {setContextFormData(val, 'brideDateOfBird')}}/>
                            <Input label='Phone number' classes={'form-control'} callBack={(val) => {setContextFormData(val, 'bridePhoneNumber')}}/>
                            <Select label='Gender' defaultValue='Choose gender' classes='form-control' callBack={(val) => {setContextFormData(val, 'brideGender')}}/>
                        </FormContent>
                        <div className='d-flex btn-box'>
                            <Button classes={'btn-primary btn-bg-ping'} text='Next' collBack={() => {history.push('/step-two')}}/>
                        </div>
                    </div>
                </Section>
            </div>
        </div>
    );
}

export default StepOne;