import React from 'react';

import './style.css';
import {Button} from "../formElements";

const Modal = (props) => {
    return (
        <div>
            <div className='modal-cover' style={props.show ? {display: "block"} : {display: "none"}}></div>
            <div className="modal-wrapper"
                 style={{
                     transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0'
                 }}>
                <div className='modal-section'>
                    <div className="mt-3">
                        <img className='modal-image' src='https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/The_Event_2010_Intertitle.svg/274px-The_Event_2010_Intertitle.svg.png'/>
                    </div>
                    <div className='mt-3'>
                        <h3><b>Event Created</b></h3>
                        <p className='mt-2'>
                            your event was successfully added to your schedule
                        </p>
                    </div>
                    <div className="modal-footer text-center d-flex-align-center">
                        <Button classes='btn-primary btn-bg-ping' text='Close' collBack={props.close}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal;