import React from 'react';

function Section({children}) {
    return (
        <div className="section">
            {children}
        </div>
    );
}

export default Section;