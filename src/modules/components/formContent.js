import React from 'react';

function FormContent({children, title, classes = ''}) {
    return (
        <div className='form-section d-flex-column'>
            <h4 className='form-title'>{title}</h4>
            <div className={`d-flex-wrap ${classes}`}>
                {children}
            </div>
        </div>
    );
}

export default FormContent;