import React from 'react';

function Select({label, defaultValue, classes, callBack}) {
    return (
        <div className={`${classes}`}>
            <div className='d-flex-column position-relative'>
                <label>{label}</label>
                <span className='select-arrow'></span>
                <select defaultValue={1} required onChange={(e) => callBack(e.target.value)}>
                    <option value="" key={1}>{defaultValue}</option>
                    <option value={2} key={2}>One</option>
                    <option value={3} key={3}>Two</option>
                    <option value={4} key={4}>Three</option>
                </select>
            </div>
        </div>
    );
}

export default Select;