import React from 'react';

function CheckBox({text, classes = '', collBack}) {
    return (
        <label className={`checkbox-container ${classes}`}>{text}
            <input type="checkbox" onChange={() => collBack()}/>
            <span className="checkmark"></span>
        </label>
    );
}

export default CheckBox;