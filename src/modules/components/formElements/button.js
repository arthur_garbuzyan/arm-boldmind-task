import React from 'react';

function Button({text, classes, collBack}) {
    return (
        <button className={`btn ${classes}`} onClick={() => collBack()}><span>{text}</span></button>
    );
}

export default Button;