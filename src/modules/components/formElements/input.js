import React from 'react';

function Input({label, type = 'text', classes = '', callBack}) {
    return (
        <div className={`${classes}`}>
            <div className='d-flex-column'>
                <label>{label}</label>
                <input type={type} onChange={(e) => callBack(e.target.value)}/>
            </div>
        </div>
    );
}

export default Input;