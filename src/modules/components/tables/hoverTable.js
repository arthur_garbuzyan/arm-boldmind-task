import React from 'react';
import './style.scss';

function HoverTable({classes = ''}) {
    return (
        <table className='hover-table'>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Guest</th>
                    <th>Phone number</th>
                    <th>Email</th>
                    <th>Side</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Anna Smith Johnson</td>
                    <td>+374456456456</td>
                    <td>annd.smith@gmail.com</td>
                    <td>Groom</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Anna Smith Johnson</td>
                    <td>+374456456456</td>
                    <td>annd.smith@gmail.com</td>
                    <td>Groom</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Anna Smith Johnson</td>
                    <td>+374456456456</td>
                    <td>annd.smith@gmail.com</td>
                    <td>Groom</td>
                </tr>
            </tbody>
        </table>
    );
}

export default HoverTable;