import React from 'react';
import './style.scss';
import {Button} from "../formElements";

function VendorsTable({classes = ''}) {
    return (
        <div className='table-content'>
            <table className='vendor-table'>
                <tbody>
                <tr>
                    <td><img src='https://kolesa-uploads.ru/-/48d03b06-4621-4966-ba49-57a3707a6e5a/hyundai-venue-20.jpg' alt='not found'/></td>
                    <td>
                        <div className='table-text-content'>
                            <p>Unique Spa Salon and pool</p>
                            <div><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, voluptate.</span></div>
                        </div>
                    </td>
                    <td><b>12 product/services</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td><img src='https://kolesa-uploads.ru/-/48d03b06-4621-4966-ba49-57a3707a6e5a/hyundai-venue-20.jpg'/></td>
                    <td>
                        <div className='table-text-content'>
                            <p>Flowerry</p>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi blanditiis commodi magni nesciunt quo? Aliquid eius iste maiores odio voluptatum.</span>
                        </div>
                    </td>
                    <td><b>12 product/services</b></td>
                    <td><Button classes={'btn-primary btn-bg-green btn-sm to-right'} collBack={() => {}} text='Details'/></td>
                </tr>
                </tbody>
            </table>
        </div>
    );
}

export default VendorsTable;