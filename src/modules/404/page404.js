import React from 'react';

const Page400 = () => {
    return(
        <div className="position-absolute-full-center">
            <h1>404!</h1>
            <h3>Sorry, page not found</h3><br></br>
            <a  className="btn btn-primary mb-5 waves-effect waves-light" href='/'>Back to Dashboard</a>
        </div>
    );
}

export default (Page400);
