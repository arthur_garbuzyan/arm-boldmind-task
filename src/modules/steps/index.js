import React, {useEffect, useState, useRef} from 'react';
import './styles.scss';
import Section from "../components/section";
import StepsContext from "../../context/stepsContext";

function Steps({location, percents, prevStep}) {

    const param = location.url.replace('/', '');
    const [currentStep, setCurrentStep] = useState(param ? param : 'step-one');
    const [interval, setNewInterval] = useState();
    const [stepPercents, setStepPercents] = useState(percents);

    const [steps, setSteps] = useState({
        ['step-one']: {percents:18, isActive:'active'},
        ['step-two']: {percents:40, isActive:''},
        ['step-three']: {percents:75, isActive:''},
        ['step-four']: {percents:85, isActive:''},
    });

    useEffect(() => {
        setNewInterval(setInterval(() => {
            setStepPercents( stepPercents => stepPercents + 1 )
        }, 3));
    }, []);

    useEffect(() => {
        let getSteps = {...steps};

        for (let index in getSteps) {
            if (index === currentStep) {
                break
            }
            getSteps[index].isActive = 'active';
        }
        setSteps(getSteps);
        if (stepPercents > steps[currentStep].percents) {
            setTimeout(function () {
                let getSteps = {...steps};
                getSteps[currentStep].isActive = 'active';
                setSteps(getSteps);
            }, 500);
            setNewInterval(clearInterval(interval))
        }
    }, [stepPercents]);

    return (
        <div className="steps d-flex-vertical-center">
            <Section>
                <div className='d-flex-space-between position-relative'>
                    <div className="steps-line-gray position-absolute"></div>
                    <div className="steps-line-ping position-absolute" id='pingLine' style={{width: `${stepPercents}%`}}></div>
                    <div className='d-flex-column'>
                        <div className={`step-big-point${steps['step-one'].isActive}`}><span className={`step-little-point${steps['step-one'].isActive}`}/></div>
                        <div className='d-flex-column step-info'>
                            <span>Step 1</span>
                            <span>Groom and bride</span>
                        </div>
                    </div>
                    <div className='d-flex-column d-flex-horizontal-center'>
                        <div className={`step-big-point${steps['step-two'].isActive}`}><span className={`step-little-point${steps['step-two'].isActive}`}/></div>
                        <div className='d-flex-column step-info text-center'>
                            <span>Step 2</span>
                            <span>Event description</span>
                        </div>
                    </div>
                    <div className='d-flex-column d-flex-horizontal-center'>
                        <div className={`step-big-point${steps['step-three'].isActive}`}><span className={`step-little-point${steps['step-three'].isActive}`}/></div>
                        <div className='d-flex-column step-info text-center'>
                            <span>Step 3</span>
                            <span>Guest list</span>
                        </div>
                    </div>
                    <div className='d-flex-column d-flex-horizontal-center'>
                        <div className={`step-big-point`}><span className={`step-little-point`}/></div>
                        <div className='d-flex-column step-info text-right'>
                            <span>Step 4</span>
                            <span>Vendors</span>
                        </div>
                    </div>
                </div>
            </Section>
        </div>
    );
}

export default Steps;